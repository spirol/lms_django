# Generated by Django 3.2.11 on 2022-01-29 06:59

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('students', '0009_alter_student_enroll_date'),
    ]

    operations = [
        migrations.AlterField(
            model_name='student',
            name='enroll_date',
            field=models.DateField(default=datetime.datetime(2022, 1, 29, 8, 58, 41, 348629)),
        ),
    ]
