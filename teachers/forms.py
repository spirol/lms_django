import django_filters
from django.core.exceptions import ValidationError
from django.forms import ModelForm
from teachers.models import Teacher


class TeacherFilter(django_filters.FilterSet):
    class Meta:
        model = Teacher
        fields = {
            'first_name': ['iexact'], 'last_name': ['iexact'],
            'experience': ['exact']
        }


class TeacherBaseForm(ModelForm):
    class Meta:
        model = Teacher
        fields = '__all__'


class TeacherCreateForm(TeacherBaseForm):
    pass


class TeacherUpdateForm(TeacherBaseForm):
    class Meta:
        model = Teacher
        exclude = ['birth_date']
