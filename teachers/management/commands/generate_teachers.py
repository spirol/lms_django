from django.core.management.base import BaseCommand
from teachers.models import Teacher


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('count', type=int, help=u'Количество создаваемых пользователей')

    def handle(self, *args, **options):
        Teacher.generate_teacher(int(options['count']))
        self.stdout.write(f'teachers added to base')
