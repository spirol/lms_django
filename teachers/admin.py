from django.contrib import admin  # noqa

# Register your models here.
from teachers.models import Teacher, Course


class TeacherAdmin(admin.ModelAdmin):
    list_per_page = 10
    list_display = ['id', 'name', 'experience']
    search_fields = ['first_name', 'last_name', 'experience']


admin.site.register(Teacher, TeacherAdmin)
admin.site.register(Course)
