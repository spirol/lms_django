from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.paginator import Paginator
from django.shortcuts import render, get_object_or_404
from django.urls import reverse, reverse_lazy
from django.views.decorators.csrf import csrf_exempt
from django.views.generic import UpdateView, DeleteView, CreateView, ListView

from core_lms.views import EditView
from teachers.models import Teacher
from teachers.forms import TeacherCreateForm, TeacherUpdateForm, TeacherFilter


class TeacherListView(LoginRequiredMixin, ListView):
    model = Teacher
    template_name = 'teachers/list_teacher.html'

    def get_filter(self, queryset=None):
        if not queryset:
            queryset = self.get_queryset()
        return TeacherFilter(data=self.request.GET, queryset=queryset)

    def get_queryset(self):
        queryset = super().get_queryset()
        queryset = queryset.select_related('group__headman').order_by('-id')
        filter_ = self.get_filter(queryset)
        return filter_.qs

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['filter'] = self.get_filter()

        paginator = Paginator(self.get_queryset(), 10)
        page_number = self.request.GET.get('page', '1')
        page_obj = paginator.page(int(page_number))
        context['page_obj'] = page_obj

        return context

# def get_teachers(request):
#     qs = Teacher.objects.all()
#
#     qs = qs.order_by('-id')
#     teacher_filter = TeacherFilter(data=request.GET, queryset=qs)
#     return render(request, 'teachers/list_teacher.html', {'args': request.GET, 'filter': teacher_filter})


class TeacherCreateView(LoginRequiredMixin, CreateView):
    model = Teacher
    success_url = reverse_lazy('teachers:list_teachers')
    form_class = TeacherUpdateForm
    template_name = 'teachers/create_teacher.html'
    pk_url_kwarg = 'id'


class TeacherEditView(LoginRequiredMixin, UpdateView):
    model = Teacher
    success_url = reverse_lazy('teachers:list_teachers')
    form_class = TeacherUpdateForm
    template_name = 'teachers/edit_teacher.html'
    pk_url_kwarg = 'id'


class TeacherDeleteView(LoginRequiredMixin, DeleteView):
    model = Teacher
    success_url = reverse_lazy('teachers:list_teachers')
    template_name = 'teachers/delete_teacher.html'
    pk_url_kwarg = 'id'
