import datetime
import re

from django.core.validators import MinValueValidator, RegexValidator, EmailValidator
from django.db import models
from faker import Faker
import random

# Create your models here.
from core_lms.models import Person


class Teacher(Person):
    category = models.IntegerField(null=False)
    experience = models.IntegerField(max_length=2, validators=[
        MinValueValidator(3)
    ])
    birth_date = models.DateField(null=True)
    inn = models.PositiveIntegerField(unique=True, null=True)
    group = models.ForeignKey(
        to='groups.Group',
        null=True,
        on_delete=models.PROTECT,
        related_name='teachers'
    )
    course = models.ManyToManyField(
        to='teachers.Course',
        related_name='courses'
    )

    @classmethod
    def generate_teacher(cls, count):
        faker = Faker()
        for i in range(count):
            s = Teacher()
            s.first_name = faker.first_name()
            s.last_name = faker.last_name()
            s.category = random.randint(1, 3)
            s.experience = random.randint(5, 30)

            s.save()

    def __str__(self):
        return f"Teacher({self.id} {self.first_name} {self.last_name} {self.category}" \
               f" {self.experience} {self.email} {self.birth_date} {self.phone_number})"

    @property
    def name(self):
        return f'{self.first_name} {self.last_name}'

