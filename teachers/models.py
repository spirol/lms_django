import datetime

from django.core.validators import MinValueValidator, RegexValidator, EmailValidator
from django.db import models
from faker import Faker
import random


# Create your models here.

class Teacher(models.Model):
    first_name = models.CharField(max_length=40, null=False)
    last_name = models.CharField(max_length=40, null=False)
    specialization = models.CharField(max_length=50, null=False)

    class CategoryLevel(models.IntegerChoices):
        TEACHER = 1, 'Teacher'
        MENTOR = 2, 'Mentor'

    category = models.PositiveIntegerField(default=CategoryLevel.TEACHER,
                                           choices=CategoryLevel.choices)

    # category = models.IntegerField(null=False)

    experience = models.IntegerField(validators=[
        MinValueValidator(3)
    ])
    email = models.EmailField(max_length=64, validators=[
        RegexValidator(
            r'^(?!.*@(\w+\.ru|mail\.net)$)',
            message='Invalid domain. Enter another email'
        )
    ])

    birth_date = models.DateField(null=True)
    phone_number = models.CharField(max_length=24)
    inn = models.PositiveIntegerField(unique=True, null=True)
    group = models.ForeignKey(
        to='groups.Group',
        null=True,
        on_delete=models.PROTECT,
        related_name='models'
    )

    @classmethod
    def generate_teacher(cls, count):
        faker = Faker()
        for i in range(count):
            s = Teacher()
            s.first_name = faker.first_name()
            s.last_name = faker.last_name()
            s.category = random.randint(1, 3)
            s.experience = random.randint(5, 30)

            s.save()

    def __str__(self):
        return f"Teacher({self.id} {self.first_name} {self.last_name} {self.specialization} {self.category}" \
               f" {self.experience} {self.email} {self.birth_date} {self.phone_number})"

    @property
    def name(self):
        return f'{self.first_name} {self.last_name}'
